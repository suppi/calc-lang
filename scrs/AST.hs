
Letrec
  [("exponent",
    Fun ["n","expn"]
        (If
          (CheckIs IsZero (Var "expn"))
          (Atom 1)
          (Mul [Var "n",
                FunCall
                  (Var "exponent")
                  [Var "n",Sub [Var "expn",Atom 1]]])))]
  (Add
    [FunCall (Var "exponent") [Atom 2,Atom 20000],
     FunCall (Var "exponent") [Atom 2,Atom 20001],
     FunCall (Var "exponent") [Atom 2,Atom 20002],
     FunCall (Var "exponent") [Atom 2,Atom 20003],
     FunCall (Var "exponent") [Atom 2,Atom 20004],
     FunCall (Var "exponent") [Atom 2,Atom 20005],
     FunCall (Var "exponent") [Atom 2,Atom 20006],
     FunCall (Var "exponent") [Atom 2,Atom 20007],
     FunCall (Var "exponent") [Atom 2,Atom 20008],
     FunCall (Var "exponent") [Atom 2,Atom 20009]])


