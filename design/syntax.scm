<expr> := <atom>
        | <var>
        | <let>
        | <letrec>
        | <if>
        | <lambda>
        | <apply>
        | <defmacro>
        | <cons>
        | <car>
        | <cdr>
        | <check-is>
        | <op>

<name> = String

<atom> := Nil | <integer>
<integer> := Integer

<let> := (let ((<name> <expr>)+) <expr>)

<let> := (letrec ((<name> <expr>)+) <expr>)

<if> := (if <expr> <expr> <expr>)

<lambda> := (lambda (<name>*) <expr>)

<apply> := (<expr> <expr>*)

<defmacro> := (defmacro <name> (((<name>*) <expr>)+ ))

<cons> := (cons <expr> <expr>)

<car> := (car <expr>)

<cdr> := (cdr <expr>)

<check-is> := (<is> <expr>)
<is> := is-zero?
      | is-nil?
      | is-atom?
      | is-cons?
      | is-fun?

<op> := (<math-ops> <epxr>*)
<math-ops> := +
            | -
            | *
            | /
