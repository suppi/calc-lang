{-
 - Evaluation of the AST
 -}

{-# LANGUAGE LambdaCase #-}

module Eval (evaluateExpr, evaluate) where

import Control.Applicative (pure, (<$>))
import Control.Monad (liftM)
import qualified Data.Map as M
import qualified Control.Parallel.Strategies as P

import Utils
import AST


evaluateExpr :: Expr -> Either String Expr
evaluateExpr = evaluate (M.fromList [])

evaluate :: Env -> Expr -> Either String Expr
-- Atom
evaluate _     a@(Atom _)    = pure a
evaluate _        Nil        = pure Nil
-- Var: lookup and evaluate in current environment
evaluate env     (Var str)   = do
  expr <- maybeToEither ("lookup of " ++ str ++ " in environment failed. Environment: " ++ show env) (M.lookup str env)
  evaluate env expr
-- Let: evaluate binder, add to environment and evaluate expression
evaluate env     (Let binds expr) = do
  evaluated_binds <- sequence (fmap (joinSecondEither (evaluate env)) binds `P.using` P.parList P.rdeepseq)
  evaluate (M.fromList evaluated_binds `M.union` env) expr
-- Letrec: evaluate binders in env containing all, add to environment and evaluate expression
evaluate env     (Letrec binds expr) = do
  let rec_env = M.fromList binds `M.union` env
  evaluated_binds <- sequence (fmap (joinSecondEither (evaluate rec_env)) binds `P.using` P.parList P.rdeepseq)
  evaluate (M.fromList evaluated_binds `M.union` rec_env) expr
-- If: evaluate bool, if it is (Nil) evaluate the false branch, else evaluate the truth branch
evaluate env     (If bool true false) = evaluate env bool >>= \case
  (Nil)    -> evaluate env false
  _        -> evaluate env true
-- Fun: create a closure - a pair of a function and the current environment (for lexical scope) and return it
evaluate env fun@(Fun _ _) = return (Clo $ Closure env fun)
-- Closure
evaluate _   clo@(Clo _) = pure clo
-- FunCall: evaluate fun. if it is a closure, evaluate function call, else it is an error
evaluate env (FunCall fun exps) = evaluate env fun >>= \case
  Clo clo -> funCallEval env clo exps
  x       -> Left $ "Error: " ++ show x ++ " is not a function"
-- Macros
evaluate _   (MacroDef _ _) = Left "macros are not supposed to be here"
evaluate _   (MacroVar _  ) = Left "macros are not supposed to be here"
-- Lists
evaluate env (Cons e1 e2)   = do
  e1_evaluated <- evaluate env e1 `P.using` P.rpar
  e2_evaluated <- evaluate env e2 `P.using` P.rpar
  pure $ Cons (e1_evaluated `P.using` P.rdeepseq) (e2_evaluated `P.using` P.rdeepseq)
evaluate env (Car expr)     = evaluate env expr >>= \case
  Cons e1 _ -> evaluate env e1
  new_expr  -> Left $ "Error: cannot get car of non-pair expr: " ++ show new_expr
evaluate env (Cdr expr)     = evaluate env expr >>= \case
  Cons _ e2 -> evaluate env e2
  new_expr  -> Left $ "Error: cannot get cdr of non-pair expr: " ++ show new_expr
evaluate env (CheckIs is expr) = (,) is <$> evaluate env expr >>= \case
  (IsAtom, Atom _)   -> pure $ Atom 1
  (IsFun, Clo _)     -> pure $ Atom 1
  (IsFun, Fun _ _)   -> pure $ Atom 1
  (IsCons, Cons _ _) -> pure $ Atom 1
  (IsNil, Nil)       -> pure $ Atom 1
  (IsZero, Atom 0)   -> pure $ Atom 1
  _                  -> pure Nil
-- Add, Mul, Sub and Div
evaluate env      (Add exps) = Atom . sum     <$> sequence (fmap (evalToInteger env) exps `P.using` P.parList P.rdeepseq)
evaluate env      (Mul exps) = Atom . product <$> sequence (fmap (evalToInteger env) exps `P.using` P.parList P.rdeepseq)
evaluate env      (Sub exps) = Atom . sub     <$> sequence (fmap (evalToInteger env) exps `P.using` P.parList P.rdeepseq)
evaluate env expr@(Div exps) = liftM  Atom     $  sequence (fmap (evalToInteger env) exps `P.using` P.parList P.rdeepseq) >>= reportDivError . divide
  where reportDivError (Left x)  = Left $ x ++ ": " ++ show expr
        reportDivError (Right x) = Right x


-- evaluate a function call.
-- check arity, evaluate arguments, add them to the environment and evaluate the body in the new environment
funCallEval :: Env -> Closure -> [Expr] -> Either String Expr
funCallEval env (Closure clo_env (Fun args body)) exps = do
  let [length_args, length_exps] = [length args, length exps] `P.using` P.parList P.rseq
  if length_args /= length_exps
    then Left $ "Error: function bad arity. got " ++ show length_exps ++ " arguments, expecting " ++ show length_args
    else do
      evaluated_exps <- sequence (fmap (evaluate env) exps `P.using` P.parList P.rdeepseq)
      let args_map = zip args evaluated_exps
      evaluate (M.fromList args_map `M.union` clo_env) body
funCallEval _ (Closure _ expr) _ = Left $ "cannot call non function: " ++ show expr


exprToInteger :: Expr -> Either String Integer
exprToInteger (Atom i) = return i
exprToInteger x = Left $ "Error: " ++ show x ++ " is not an integer"

evalToInteger :: Env -> Expr -> Either String Integer
evalToInteger env e = exprToInteger =<< evaluate env e

