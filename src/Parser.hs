module Parser (parseProg) where

import qualified Text.Parsec as P
import qualified Lexer as L
import Text.ParserCombinators.Parsec hiding (spaces)

import AST

parseAtom :: Parser Expr
parseAtom = do
  s <- optionMaybe $ char '-'
  whole <- many1 digit
  let num = read (sign s++whole) :: Integer
  return (Atom  num)

sign ::  Maybe t -> String
sign s = case s of { Nothing -> ""; Just _ -> "-" }


parseVar :: Parser Expr
parseVar = do
  name <- L.identifier
  return (Var name)

parseLet :: Parser Expr
parseLet = L.parens $ do
  L.reserved "let"
  args <- parseLetargs
  expr <- parseExpr
  return (Let args expr)

parseLetrec :: Parser Expr
parseLetrec= L.parens $ do
  L.reserved "letrec"
  args <- parseLetargs
  expr <- parseExpr
  return (Letrec args expr)

parseLetargs :: Parser [(String,Expr)]
parseLetargs = L.brackets $ P.sepBy parseLetarg P.spaces


parseLetarg :: Parser (String,Expr)
parseLetarg = L.parens $ do
  name <- L.identifier
  expr <- parseExpr
  return (name,expr)

parseAdd :: Parser Expr
parseAdd = L.parens $ do
  L.reservedOp "+"
  expr <- P.sepBy parseExpr P.spaces
  return (Add expr)

parseSub :: Parser Expr
parseSub = L.parens $ do
  L.reservedOp "-"
  expr <- P.sepBy parseExpr P.spaces
  return (Sub expr)

parseMul :: Parser Expr
parseMul = L.parens $ do
  L.reservedOp "*"
  expr <- P.sepBy parseExpr P.spaces
  return (Mul expr)

parseDiv :: Parser Expr
parseDiv = L.parens $ do
  L.reservedOp "/"
  expr <- P.sepBy parseExpr P.spaces
  return (Div expr)

parseIf :: Parser Expr
parseIf = L.parens $ do
  L.reserved "if"
  expr1 <- parseExpr
  P.spaces
  expr2 <- parseExpr
  P.spaces
  expr3 <- parseExpr
  return (If expr1 expr2 expr3)

parseFun :: Parser Expr
parseFun = L.parens $ do
  L.reserved "lambda"
  args <- L.parens $ P.sepBy L.identifier P.spaces
  expr <- parseExpr
  return (Fun args expr)

parseNil :: Parser Expr
parseNil = do
  L.reserved "nil"
  return Nil

parseCar :: Parser Expr
parseCar = L.parens $ do
  L.reserved "car"
  expr <- parseExpr
  return (Car expr)

parseCdr :: Parser Expr
parseCdr = L.parens $ do
  L.reserved "cdr"
  expr <- parseExpr
  return (Cdr expr)

parseCons :: Parser Expr
parseCons = L.parens $ do
  L.reserved "cons"
  expr1 <- parseExpr
  expr2 <- parseExpr
  return (Cons expr1 expr2)

parseFunCall :: Parser Expr
parseFunCall = L.parens $ do
  name <- parseExpr
  expr <- P.sepBy parseExpr P.spaces
  return (FunCall name expr)

parseIsZero :: Parser Expr
parseIsZero = L.parens $ do
  L.reservedOp "is-zero?"
  expr <- parseExpr
  return (CheckIs IsZero expr)

parseIsAtom :: Parser Expr
parseIsAtom = L.parens $ do
  L.reservedOp "is-atom?"
  expr <- parseExpr
  return (CheckIs IsAtom expr)

parseIsFun :: Parser Expr
parseIsFun = L.parens $ do
  L.reservedOp "is-fun?"
  expr <- parseExpr
  return (CheckIs IsFun expr)

parseIsNil :: Parser Expr
parseIsNil = L.parens $ do
  L.reservedOp "is-nil?"
  expr <- parseExpr
  return (CheckIs IsNil expr)

parseIsCons :: Parser Expr
parseIsCons = L.parens $ do
  L.reservedOp "is-cons?"
  expr <- parseExpr
  return (CheckIs IsCons expr)


parseExpr :: Parser Expr
parseExpr = try parseAtom
  <|> try parseIf
  <|> try parseIsZero
  <|> try parseIsCons
  <|> try parseIsNil
  <|> try parseIsAtom
  <|> try parseIsFun
  <|> try parseVar
  <|> try parseLet
  <|> try parseLetrec
  <|> try parseAdd
  <|> try parseSub
  <|> try parseMul
  <|> try parseDiv
  <|> try parseFun
  <|> try parseNil
  <|> try parseCar
  <|> try parseCdr
  <|> try parseCons
  <|> try parseFunCall

parseProg :: FilePath -> String -> Either String Expr
parseProg filePath fileContent = case parse parseExpr filePath fileContent of
  Left err  -> Left $ show err
  Right res -> return res
