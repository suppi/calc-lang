module Run where

import Parser
import Eval

run :: FilePath -> String -> String
run path content =
  case parseProg path content >>= evaluateExpr of
    Left err  -> err
    Right res -> show res

toAST :: FilePath -> String -> String
toAST path content = case parseProg path content of
  Right x -> show x
  Left x  -> show x


