{-
 - Defines the AST of the language
 -}

{-# LANGUAGE DeriveGeneric #-}

module AST where

import qualified Data.Map as M
import Control.DeepSeq (NFData)
import GHC.Generics (Generic)

data Expr = Atom Integer
          | Var String
          | Let    [(String,Expr)] Expr
          | Letrec [(String,Expr)] Expr
          | If Expr Expr Expr
          | Fun [String] Expr
          | FunCall Expr [Expr]
          | Clo Closure
          | MacroDef String [([String], Expr)]
          | MacroVar String
          | Cons Expr Expr
          | Car Expr
          | Cdr Expr
          | CheckIs Is Expr
          | Nil
          | Add [Expr]
          | Mul [Expr]
          | Sub [Expr]
          | Div [Expr] deriving (Eq, Show, Generic)

data Is = IsAtom
        | IsFun
        | IsCons
        | IsNil
        | IsZero deriving (Eq, Show)

instance NFData Expr


data Closure = Closure Env Expr deriving (Eq, Show)
type Env = M.Map String Expr

