{-
 - Preprocessing the AST
 -}

{-# LANGUAGE LambdaCase #-}

module Preprocess (preprocessExpr, preprocess) where

import Control.Applicative ((<$>), (<*>))
import qualified Data.Map as M

import Utils
import AST

preprocessExpr :: Expr -> Either String Expr
preprocessExpr = preprocess (M.fromList [])

preprocess :: Env -> Expr -> Either String Expr
-- Atom
preprocess _   a@(Atom _)  = return a
-- Nil
preprocess _      Nil      = return Nil
-- Var
preprocess _   v@(Var _) = return v
-- MacroVar
preprocess env (MacroVar name) = maybeToEither ("Error: Couldn't find variable " ++ name ++ " in environment: " ++ show env) (M.lookup name env)
-- Let: preprocess bind and expr
preprocess env     (Let binds expr) = Let <$> mapM (joinSecondEither (preprocess env)) binds <*> preprocess env expr
preprocess env     (Letrec binds expr) = Letrec <$> mapM (joinSecondEither (preprocess env)) binds <*> preprocess env expr
-- If: preprocess bool true and false
preprocess env     (If bool true false) = If <$> preprocess env bool <*> preprocess env true <*> preprocess env false
-- Fun:
preprocess env     (Fun args expr) = Fun args <$> preprocess env expr
-- Closure
preprocess _    clo@(Clo _) = return clo
-- Add, Mul, Sub and Div
preprocess env (Add exps) = Add <$> mapM (preprocess env) exps
preprocess env (Mul exps) = Mul <$> mapM (preprocess env) exps
preprocess env (Sub exps) = Sub <$> mapM (preprocess env) exps
preprocess env (Div exps) = Div <$> mapM (preprocess env) exps
-- MacroDef
preprocess env mac@(MacroDef name _) = return $ Clo $ Closure (M.insert name mac env) mac
-- FunCall
preprocess env (FunCall mac args) = preprocess env mac >>= \case
  Clo clo -> macroCallProcess env clo args
  fun     -> FunCall <$> return fun <*> mapM (preprocess env) args
preprocess env (Cons e1 e2) = Cons <$> preprocess env e1 <*> preprocess env e2
preprocess env (Car   expr) = Car  <$> preprocess env expr
preprocess env (Cdr   expr) = Cdr  <$> preprocess env expr
preprocess env (CheckIs i expr) = CheckIs i <$> preprocess env expr

macroCallProcess :: Env -> Closure -> [Expr] -> Either String Expr
macroCallProcess _ (Closure _ (MacroDef name [])) _ = Left $ "No match found for macro " ++ name
macroCallProcess env (Closure clo_env (MacroDef name ((args,body):rest))) exps =
  if length args /= length exps
  then macroCallProcess env (Closure clo_env (MacroDef name rest)) exps
  else do
    preprocessed_exps <- mapM (preprocess env) exps
    let args_map = zip args preprocessed_exps
    preprocess (M.fromList args_map `M.union` clo_env) body

macroCallProcess env cloj _ = Left $ "Could not process macro: " ++ show cloj ++ ", in environment: " ++ show env
