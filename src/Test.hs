{-
 - Tests
 -}

{-# LANGUAGE LambdaCase #-}

module Test (runTests, runParTests, test) where

import qualified Data.Map as M

import AST
import Preprocess
import Eval

showResult :: Either String Expr -> String
showResult (Left str) = str
showResult (Right (Atom x)) = show x
showResult (Right x) = show x

showPassFail :: Either String Expr -> Either String Expr -> String
showPassFail result expected
  | result == expected = "   Passed. Result: "
  | otherwise = "X  Failed. expected: " ++ showResult expected ++ ", result: "

test :: Expr -> Either String Expr -> IO ()
test ex res = do
  let tst = preprocessExpr ex >>= evaluateExpr
  putStrLn $ showPassFail tst res ++ showResult tst

runTests :: IO ()
runTests = do
  -- test1
  let ex1 = Mul [Add [Atom 5, Atom 6], Div [Atom 7, Sub [Atom 5, Atom 5]]]
  test ex1 (Left "Error - cannot divide by zero: Div [Atom 7,Sub [Atom 5,Atom 5]]")
  -- test2
  let ex2 = Sub [Mul [Atom 792, Sub [Atom 91, Atom 3]], Div [Atom 1888, Atom 2]]
  test ex2 (Right (Atom 68752))
  -- test3
  let ex3 = Sub [Mul [Var "x", Sub [Atom 91, Atom 3]], Div [Atom 1888, Atom 2]]
  let test3a = evaluate (M.fromList [("x", Atom 792)]) ex3
  putStrLn $ showPassFail test3a (Right (Atom 68752)) ++ showResult test3a
  test ex3 (Left "lookup of x in environment failed. Environment: fromList []")
  -- test4
  let ex4 = Let [("x", Atom 792)] (Sub [Mul [Var "x", Sub [Atom 91, Atom 3]], Div [Atom 1888, Atom 2]])
  test ex4 (Right (Atom 68752))
  -- test5
  let ex5 = FunCall (Fun ["x"] (Add [Var "x", Var "x"])) [Add [Atom 1, Atom 1]]
  test ex5 (Right (Atom 4))
  let test5b = evaluate (M.fromList [("x", Atom 0)]) ex5
  putStrLn $ showPassFail test5b (Right (Atom 4)) ++ showResult test5b
  -- test6
  let ex6 = FunCall (Let [("x", Atom 0)] (Fun ["y"] (Add [Var "x", Var "y", Var "y"]))) [Let [("x", Atom 3)] (Add [Var "x", Atom 3])]
  test ex6 (Right (Atom 12))
  -- test7
  let ex7 = Let [("double", Fun ["x"] (Mul [Atom 2,Var "x"]))] (Add [Let [("x", Atom 5)] (FunCall (Var "double") [Mul [Var "x",Var "x"]]),Let [("x", Atom 1)] (Var "x")])
  let test7a = evaluate (M.fromList []) ex7
  putStrLn $ showPassFail test7a (Right (Atom 51)) ++ showResult test7a
  -- test8
  let ex8 = If (If (Add [Atom (-1), Atom 1]) (Atom 0) (Atom 139)) (Atom 17) (Atom 0)
  test ex8 (Right (Atom 17))
  -- test9
  let ex9 = FunCall (Letrec [("add", Fun ["x","y"] (If (CheckIs IsZero (Var "x")) (Var "y") (FunCall (Var "add") [Sub [Var "x", Atom 1], Add [Var "y", Atom 1]])))] (Var "add")) [Atom 10, Atom 5]
  test ex9 (Right (Atom 15))
  -- test10
  let ex10 = FunCall (Letrec [("add", Fun ["x","y"] (If (CheckIs IsZero (Var "x")) (If (CheckIs IsZero (Sub [Var "y", Atom 1])) (Atom 0) (FunCall (Var "add") [Var "y", Atom 1])) (Add [Atom 1, FunCall (Var "add") [Sub [Var "x", Atom 1], Var "y"]])))] (Var "add")) [Atom 10, Atom 5]
  test ex10 (Right (Atom 15))

  let ex11 = Letrec [("fib", Fun ["n", "prod"] (If (CheckIs IsZero (Var "n")) (Var "prod") (FunCall (Var "fib") [Sub [Var "n", Atom 1], Mul [Var "n", Var "prod"]])))] (FunCall (Var "fib") [Atom 5, Atom 1])
  test ex11 $ Right (Atom 120)

  -- Letrec test
  let ex12 = Letrec [("is-even", Fun ["n"]
                                   (If (CheckIs IsZero (Var "n")) (Atom 1)
                                   (FunCall (Var "is-odd")  [Sub [Var "n", Atom 1]])))
                    ,("is-odd",  Fun ["n"]
                                   (If (CheckIs IsZero (Var "n")) (Atom 0)
                                   (FunCall (Var "is-even") [Sub [Var "n", Atom 1]])))]
                    (FunCall (Var "is-odd") [Atom 9999])
  test ex12 $ Right (Atom 1)

  -- Macro tests
  let mac01 = FunCall (MacroDef "unless" [(["bool", "true", "false"], If (CheckIs IsZero (MacroVar "bool")) (MacroVar "true") (MacroVar "false"))]) [Atom 1,Atom 1,Atom 0]
  test mac01 (Right (Atom 0))
  let mac02 = Let [("foo", Atom 5), ("bar", Atom 7)] (FunCall (MacroDef "foo" [(["bar"], If (CheckIs IsZero (MacroVar "bar")) (Var "bar") (Var "foo"))]) [Atom 1])
  test mac02 (Right (Atom 5))

runParTests :: IO ()
runParTests = do
  --let par01 = Add [Atom $ sum [1..30000001], Atom $ sum [2..30000002], Atom $ sum [3..30000003], Atom $ sum [4..30000004]]
  --test par01 $ Right (Atom 0)
  let par02 = Letrec [("fac", Fun ["n", "prod"] (If (CheckIs IsZero (Var "n")) (Var "prod") (FunCall (Var "fac") [Sub [Var "n", Atom 1], Mul [Var "n", Var "prod"]])))] $
                Add [FunCall (Var "fac") [Atom 33700, Atom 1]
                    ,FunCall (Var "fac") [Atom 33554, Atom 1]
                    ,FunCall (Var "fac") [Atom 33553, Atom 1]
                    ,FunCall (Var "fac") [Atom 34550, Atom 1]
                    ,FunCall (Var "fac") [Atom 33250, Atom 1]
                    ,FunCall (Var "fac") [Atom 33510, Atom 1]
                    ,FunCall (Var "fac") [Atom 33530, Atom 1]
                    ,FunCall (Var "fac") [Atom 33650, Atom 1]
                    ,FunCall (Var "fac") [Atom 33554, Atom 1]
                    ,FunCall (Var "fac") [Atom 33553, Atom 1]
                    ,FunCall (Var "fac") [Atom 33552, Atom 1]
                    ,FunCall (Var "fac") [Atom 33551, Atom 1]]
  test par02 $ Right (Atom 0)
