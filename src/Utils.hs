module Utils where



maybeToEither :: a -> Maybe b -> Either a b
maybeToEither _ (Just y) = Right y
maybeToEither x Nothing  = Left x

joinSecondEither :: (a -> Either b c) -> (d, a) -> Either b (d, c)
joinSecondEither f (y,x) = case f x of
  Right z -> Right (y,z)
  Left  z -> Left z


sub :: Num a => [a] -> a
sub [] = 0 
sub xs = foldl1 (-) xs

divide :: (Eq a, Integral a) => [a] -> Either String a
divide []         = return 1
divide [x]        = return x
divide (_:0:_)    = Left "Error - cannot divide by zero"
divide (x:y:rest) = divide $ div x y : rest
