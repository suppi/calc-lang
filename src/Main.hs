module Main where

import System.Environment (getArgs)
import System.IO (stdout, hFlush)
import Control.Monad (unless)

import Run

main :: IO ()
main = do
  args <- getArgs
  case args of
    [filePath] -> readFile filePath >>= putStrLn . run filePath
    [] -> putStrLn welcomeMessage >> loop (run "REPL") >> putStrLn "Good bye."
    _  -> putStrLn usageMessage

loop :: (String -> String) -> IO ()
loop f = do
  putStr "> "
  hFlush stdout
  str <- getLine
  unless (str == ":q") (putStrLn (f str) >> loop f)


welcomeMessage ::  String
welcomeMessage =
  unlines ["Calc-lang interpreter prototype by Moti and Gil Mizrahi"
          ,"Write an expression or \":q\" to quit"]

usageMessage ::  String
usageMessage =
  unlines ["usage:"
          , "to interpret a source file: ./calc <filepath>"
          , "to run REPL: ./calc"]

